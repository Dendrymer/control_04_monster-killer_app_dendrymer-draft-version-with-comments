const ATTACK_VALUE = 10;
const STRONG_ATTACK_VALUE = 17;
const MONSTER_ATTACK_VALUE = 14;
const HEAL_VALUE = 20; 

//global identifies:
const ATTACK_MODE = 'ATTACK';
const STRONG_ATTACK_MODE = 'STRONG_ATTACK';
const LOG_EVENT_PLAYER_ATTACK = 'PLAYER_ATTACK'; 
const LOG_EVENT_PLAYER_STRONG_ATTACK = 'PLAYER_STRONG_ATTACK'; 
const LOG_EVENT_MONSTER_ATTACK = 'MONSTER_ATTACK'; 
const LOG_EVENT_PLAYER_HEAL = 'PLAYER_HEAL';
const LOG_EVENT_GAME_OVER = 'GAME_OVER';


let battleLog = [];
let lastLoggedEntry;

function getMaxLifeValues() {
  const enteredValue = prompt('Maximum value for you and the mnster.', '100')

  const parsedValue = parseInt(enteredValue);
  if (isNaN(enteredValue) || parsedValue <= 0) {
    throw { message: 'Invalid user input, not a number !'};
  }
  return parsedValue;
}

let chosenMaxLife;

try {
  chosenMaxLife = getMaxLifeValues ();
} catch (error) {
  console.log(error);
  chosenMaxLife = 100;
  alert('You entered somthing wrong, default value of 100 was used. ');
} 
//finally {
//} używasz gdyby mogło dojść do kolejnego error w twoim catch' up block of code
// kod w finally zostaje czytany z cofnięciem zmian z catch; może wysłać info do 
// ciebie z wyrzuceniem kolejnego error throw error.

let currentMonsterHelth = chosenMaxLife;
let currentPlayerHelth = chosenMaxLife; 
let hasBonusLife = true;

adjustHealthBars(chosenMaxLife);

function writeToLog (event, value, monsterHelth, playerHelth) {
  let logEntry = {
    event: event,
    value: value,
    finalMonsterHelth:monsterHelth, 
    finalPlayerHelth: playerHelth
  };
  switch (event) {
    case LOG_EVENT_PLAYER_ATTACK:
      logEntry.target = 'MONSTER';
      break;
    case LOG_EVENT_PLAYER_STRONG_ATTACK:
      logEntry.target = 'MONSTER';
      break;
    case LOG_EVENT_MONSTER_ATTACK:
      logEntry.target = 'PLAYER';
      break;
    case LOG_EVENT_PLAYER_HEAL:
      logEntry.target = 'PLAYER';
      break;
    case LOG_EVENT_GAME_OVER:
      logEntry;  
      
    break;

    default:
      logEntry = {};
  };
//switch statment- dobrze sprawdza się do zastąpienia if z warunkiem ===
//switch porównuje wartość i typ; uwaga switch to stament fall through ! potrzebny break
  //if (event === LOG_EVENT_PLAYER_ATTACK) {
  //  logEntry.target = 'MONSTER';
  //} else if (event === LOG_EVENT_PLAYER_STRONG_ATTACK) {
  //  logEntry.target = 'MONSTER';
  //} else if (event === LOG_EVENT_MONSTER_ATTACK) {
  //  logEntry.target = 'PLAYER';
  //} else if (event === LOG_EVENT_PLAYER_HEAL) {
  //  logEntry.target = 'PLAYER';
  //} // not necessery if we define object logEntry upper: 
  //else if ( event === LOG_EVENT_GAME_OVER) {
    //logEntry = {
      //event: event,
      //value: value,
      //finalMonsterHelth:monsterHelth, 
      //finalPlayerHelth: playerHelth};
  //}
    battleLog.push(logEntry);
}

function reset() {
  currentMonsterHelth = chosenMaxLife;
  currentPlayerHelth = chosenMaxLife;
  resetGame(chosenMaxLife);
}

function endRound() {
  const initialPlayerHealth = currentPlayerHelth;
  const playerDamage = dealPlayerDamage(MONSTER_ATTACK_VALUE); //dealPlayerDamage from vendor.js
  currentPlayerHelth -= playerDamage;
  writeToLog(
    LOG_EVENT_MONSTER_ATTACK,
    playerDamage, 
    currentMonsterHelth, 
    currentPlayerHelth
    );

  if (currentPlayerHelth <= 0 && hasBonusLife) {
    hasBonusLife = false; 
    removeBonusLife ()
    currentPlayerHelth = initialPlayerHealth;
    setPlayerHealth (initialPlayerHealth);
    alert('You would be dead but the bonus life saved you!');
  }

  if (currentMonsterHelth <= 0 && currentPlayerHelth > 0) {
    alert('You won!');
    writeToLog(
      LOG_EVENT_GAME_OVER,
      'YOU WON', 
      currentMonsterHelth, 
      currentPlayerHelth
      );
  } else if (currentPlayerHelth <= 0 && currentMonsterHelth > 0) {
    alert('You lost!');
    writeToLog(
      LOG_EVENT_GAME_OVER,
      'YOU LOST', 
      currentMonsterHelth, 
      currentPlayerHelth
      );
  } else if (currentPlayerHelth <= 0 && currentMonsterHelth <= 0) {
    alert('Both dead :c');
    writeToLog(
      LOG_EVENT_GAME_OVER,
      'A DRAW', 
      currentMonsterHelth, 
      currentPlayerHelth
      );
  }

  if (currentMonsterHelth <= 0 || currentPlayerHelth <= 0) {
    reset();
  }
}

function attackMonster(mode) {
  const maxDamage = mode === ATTACK_MODE ? ATTACK_VALUE : STRONG_ATTACK_VALUE;
  const logEntry = 
  mode === ATTACK_MODE 
  ? LOG_EVENT_PLAYER_ATTACK 
  : LOG_EVENT_PLAYER_STRONG_ATTACK;
  //let maxDamage;
  //let logEntry;
  //if (mode === ATTACK_MODE) {
  //  maxDamage = ATTACK_VALUE;
  //  logEntry = LOG_EVENT_PLAYER_ATTACK;
  //} else if (mode = STRONG_ATTACK_MODE) {
  //  maxDamage = STRONG_ATTACK_VALUE;
  //  logEntry = LOG_EVENT_PLAYER_STRONG_ATTACK
  //}
  const damage = dealMonsterDamage(maxDamage); //dealMonsterDamage from vendor.js
  currentMonsterHelth -= damage;
  writeToLog(
    logEntry,
    damage,
    currentMonsterHelth,
    currentPlayerHelth,
  );
  endRound();
}

function attackHandler() {
  attackMonster(ATTACK_MODE); 
}

function strongAttackHendler() {
 attackMonster(STRONG_ATTACK_MODE)
}

function healPlayerHandler() {
  let healValue;
  if (currentPlayerHelth >= chosenMaxLife - HEAL_VALUE) {
    alert("You can't heal to more than your max initial health.");
    healValue = chosenMaxLife - currentPlayerHelth;
  } else {
    healValue = HEAL_VALUE
  }
  writeToLog(
    LOG_EVENT_PLAYER_HEAL,
    healValue,
    currentMonsterHelth,
    currentPlayerHelth,
  );
  
  increasePlayerHealth(HEAL_VALUE); //increasePlayerHealth from vendor.js 
  //(uaktualnia tylko pasek graficzny życia)
  currentPlayerHelth += HEAL_VALUE;
  
  endRound();
}

function printLogHandler() {
  //Przykładowe dwie petla for:
  for (let i = 0; i < 2; i++) {
    console.log('------');
  }
  // for (let i = 10; i > 0; ){
  //   i--;
  //   console.log(i);
  // } -ta wersja zaczyna wyświetlać liczby od 9
  
  // petla while oraz do while przyklady + labeled statment
  //let j = 0;
  //while ( j < 3) {
  //  console.log(j);
  //  j++;

  //let j = 0; 
  //outerWhile: do { //outerWhile- is labell of do while statement
  //  console.log('Outer', j);
  //  innerFor: for (let k = 0; k < 5; k++){
  //    if (k === 3) {
  //      break outerWhile; // so i can break specific labeled loop e.g.
  //      // continue outerWhile; => create Infinite loop in that case !!
  //    }
  //    console.log('Inner', k )
  //  }
  //  j++;
  //} while (j < 3); 
  

  //petla for, for of -Arays, for in -Objects:
  //for (let i = 0; i < battleLog.length; i++){
  //  console.log(battleLog [i])
  //} - wyświetla mi każdy element Aray osobno dzięki loop for
  
  //let i = 0
  //for (const logEntry of battleLog) {
  //  console.log(logEntry); // tworzy const osobno dla każdej iteracji
  //   i wyświetla każde logEntry osobno, ten format działa tylko
  //   w przypadku Arays i też dla string- ma Arays charackter ?
  //   numer iteracji tżeba dodać ręcznie 
  //  console.log(i);
  //  i++;

  let i = 0;
  for (const logEntry of battleLog) {
    if (!lastLoggedEntry && lastLoggedEntry !== 0 || lastLoggedEntry < i) {
      console.log( `#${i}`);
      for (const key in logEntry) {
      console.log(`${key} => ${logEntry[key]}`);
      }
      lastLoggedEntry = i;
      break;
    }
    i++;
  }
} 

attackBtn.addEventListener('click', attackHandler);
strongAttackBtn.addEventListener('click', strongAttackHendler);
healBtn.addEventListener('click', healPlayerHandler);
logBtn.addEventListener('click', printLogHandler); 

//let sum = 0;
//for (let i = 0; i <2; i++){
//  for (let j =5; j > 2; j--){
//    sum= sum + i + j 
//}
//}
//console.log(sum); 

